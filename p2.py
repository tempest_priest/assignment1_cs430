import key_gen
import argparse
import sys
import json

from hashlib import md5
from base64 import b64decode
from base64 import b64encode

from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad


class AESCipher:
    def __init__(self, kkey=None, mode='cbc'):
        self.key = md5(kkey.encode('utf8')).digest()
        self.mode = mode
        self.cipher = None

    def encrypt(self, message=None):
        if message is not None:
            if self.mode == 'cbc':
                self.cipher = AES.new(self.key, AES.MODE_CBC)
                p_msg = pad(bytes(message, 'utf-8'), AES.block_size)
                ct_bytes = self.cipher.encrypt(p_msg)
                iv = b64encode(self.cipher.iv).decode('utf-8')
                ct = b64encode(ct_bytes).decode('utf-8')
                result = json.dumps({'iv': iv, 'ciphertext': ct})
                return result
            elif self.mode == 'ctr':
                self.cipher = AES.new(self.key, AES.MODE_CTR)
                ct_bytes = self.cipher.encrypt(bytes(message, 'utf-8'))
                nonce = b64encode(self.cipher.nonce).decode('utf-8')
                ct = b64encode(ct_bytes).decode('utf-8')
                result = json.dumps({'nonce': nonce, 'ciphertext': ct})
                return result
            else:
                print("Please re-enter a proper mode: cbc or ctr")
                return None

        else:
            print("Please give a non-empty message... ")
            return None

    def decrypt(self, ccipher=None):
        if ccipher is not None:

            if self.mode == 'cbc':
                b64 = json.loads(ccipher)
                iv = b64decode(b64['iv'])
                ct = b64decode(b64['ciphertext'])
                self.cipher = AES.new(self.key, AES.MODE_CBC, iv)
                plain_text = unpad(self.cipher.decrypt(ct), AES.block_size)
                return plain_text

            elif self.mode == 'ctr':
                b64 = json.loads(ccipher)
                nonce = b64decode(b64['nonce'])
                ct = b64decode(b64['ciphertext'])
                self.cipher = AES.new(self.key, AES.MODE_CTR, nonce=nonce)
                plain_text = self.cipher.decrypt(ct)
                return plain_text
            else:
                print("Please re-enter a proper mode: cbc or ctr")
                return None
        else:
            print("Please give a non-empty cipher... ")
            return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--operation", "-o", type=str, help="the task: encrypt or decrypt")
    parser.add_argument("--input", "-i", help="the message to be encrypted or the cipher to be decrypted")
    parser.add_argument("--decrypt_key", "-d", type=str, help="the key using which we decrypt the cipher")
    parser.add_argument("--mode", "-m", type=str, help="CBC or CTR mode.")
    args = parser.parse_args()
    if len(sys.argv) > 1:
        if args.operation == "encrypt":
            msg = args.input
            key_len = 16
            k = key_gen.KeyGen(length=key_len)
            k.get_keys()
            print("key generated is: ", k.key)
            key = k.key
            aes_obj = AESCipher(kkey=key)
            encrypted_msg = aes_obj.encrypt(message=msg)
            print("Encrypted msg in bytes: ", encrypted_msg)
        elif args.operation == "decrypt" and args.decrypt_key is not None:
            cipher = args.input
            key = args.decrypt_key
            print("Now decrypting: ")
            aes_obj = AESCipher(kkey=key)
            decrypted_msg = aes_obj.decrypt(ccipher=cipher)
            print("Decrypted msg: ", bytes(decrypted_msg).decode('utf-8'))
        else:
            print("Invalid operation")
    else:
        print()
        print("You have not passed any commandline parameters. Therefore running default encryption and decryption "
              "processes on default message.")
        default_msg = "Let there be peace!"
        print("Default message to be encrypted: ", default_msg, sep='\n')
        print()

        # CBC encryption part.

        print("First: Demonstration of CBC encryption and decryption.")
        k = key_gen.KeyGen(length=16)
        k.get_keys()
        print("key generated is: ", k.key)
        key = k.key
        aes_obj = AESCipher(kkey=key, mode='cbc')
        encrypted_msg = aes_obj.encrypt(default_msg)
        print("Encrypted msg in bytes: ", encrypted_msg)

        # CBC decryption part.

        cipher = encrypted_msg
        print("Now decrypting: ")
        aes_obj = AESCipher(kkey=key, mode='cbc')
        decrypted_msg = aes_obj.decrypt(ccipher=cipher)
        print("Decrypted msg: ", bytes(decrypted_msg).decode('utf-8'))
        print()

        # CTR encryption part.

        print("Second: Demonstration of CTR encryption and decryption.")
        k = key_gen.KeyGen(length=16)
        k.get_keys()
        print("key generated is: ", k.key)
        ctr_key = k.key
        aes_obj = AESCipher(kkey=ctr_key, mode='ctr')
        encrypted_msg = aes_obj.encrypt(default_msg)
        print("Encrypted msg in bytes: ", encrypted_msg)

        # CTR decryption part.

        cipher = encrypted_msg
        print("Now decrypting: ")
        aes_obj = AESCipher(kkey=ctr_key, mode='ctr')
        decrypted_msg = aes_obj.decrypt(ccipher=cipher)
        print("Decrypted msg: ", bytes(decrypted_msg).decode('utf-8'))
        print()


