import key_gen
import argparse
import ast
import sys


class OneTimePad:
    def __init__(self, filename="key.txt", key=None):
        self.filename = filename
        self.key = key

    def encrypt(self, message=None):
        if message is not None:
            if len(self.key) == len(message):
                encrypted = [a ^ b for (a, b) in zip(bytes(message, 'utf-8'), bytes(self.key, 'utf-8'))]
                return encrypted
            else:
                print("Please make sure key length matches message length.")
                return None
        else:
            print("Please give a non-empty message... ")
            return None

    def decrypt(self, cipher=None):
        if cipher is not None:
            if len(self.key) == len(cipher):
                encrypted = [a ^ b for (a, b) in zip(cipher, bytes(self.key, 'utf-8'))]
                return encrypted
            else:
                print("Please make sure key length matches cipher length.")
                return None
        else:
            print("Please give a non-empty cipher... ")
            return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--operation", "-o", type=str, help="the task: encrypt or decrypt")
    parser.add_argument("--input", "-i", help="the message to be encrypted or the cipher to be decrypted")
    parser.add_argument("--decrypt_key", "-d", type=str, help="the key using which we decrypt the cipher")
    args = parser.parse_args()
    if len(sys.argv) > 1:
        if args.operation == "encrypt":
            print()
            msg = args.input
            msg_len = len(msg)
            k = key_gen.KeyGen(length=msg_len)
            k.get_keys()
            print("key generated is: ", k.key)
            key = k.key
            otp = OneTimePad(key=key)
            encrypted_msg = otp.encrypt(msg)
            print("Encrypted msg in bytes: ", encrypted_msg)
            print()
        elif args.operation == "decrypt" and args.decrypt_key is not None:
            print()
            cipher = ast.literal_eval(args.input)
            key = args.decrypt_key
            print("Now decrypting: ")
            otp = OneTimePad(key=key)
            decrypted_msg = otp.decrypt(cipher=cipher)
            print("Decrypted msg: ", bytes(decrypted_msg).decode('utf-8'))
            print()
        else:
            print("Invalid operation. Choose either \'encrypt\' or \'decrypt\'.")
    else:
        print()
        print("You have not passed any commandline parameters. Therefore running default encryption and decryption "
              "processes on default message.")
        default_msg = "Let there be peace!"
        print("Default message to be encrypted: ", default_msg, sep='\n')

        # encryption part.
        msg_len = len(default_msg)
        k = key_gen.KeyGen(length=msg_len)
        k.get_keys()
        print("key generated is: ", k.key)
        key = k.key
        otp = OneTimePad(key=key)
        encrypted_msg = otp.encrypt(default_msg)
        print("Encrypted msg in bytes: ", encrypted_msg)

        # decryption part.

        cipher = encrypted_msg
        key = k.key
        print("Now decrypting: ")
        otp = OneTimePad(key=key)
        decrypted_msg = otp.decrypt(cipher=cipher)
        print("Decrypted msg: ", bytes(decrypted_msg).decode('utf-8'))
        print()
