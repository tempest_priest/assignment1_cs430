import key_gen
import argparse
import sys
import json

from hashlib import md5
from base64 import b64encode

from Crypto.Cipher import AES
from Crypto.Util.Padding import pad


class CBC_MAC:
    def __init__(self, kkey=None):
        self.key = md5(kkey.encode('utf8')).digest()
        self.cipher = None
        self.iv = bytes('0'*16, 'utf-8')
        self.ecb_key = md5('hpawmyOPrJvTvt39'.encode('utf8')).digest()

    def generate_tag(self, message=None):
        if message is not None:
            p_msg = pad(bytes(message, 'utf-8'), AES.block_size)
            self.cipher = AES.new(self.key, AES.MODE_CBC, iv=self.iv)
            ct_bytes = self.cipher.encrypt(p_msg)

            # now perform ecb encryption
            ecb_obj = AES.new(self.ecb_key, AES.MODE_ECB)
            tag_bytes = ecb_obj.encrypt(ct_bytes)
            tag = b64encode(tag_bytes).decode('utf-8')
            result = json.dumps({'plain_text': message, 'tag': tag})
            return result

        else:
            print("Please give a non-empty message... ")
            return None

    def verify_tag(self, ccipher=None):
        if ccipher is not None:
            b64 = json.loads(ccipher)
            received_tag = b64['tag']
            message = b64['plain_text']

            # verification process.
            p_msg = pad(bytes(message, 'utf-8'), AES.block_size)
            self.cipher = AES.new(self.key, AES.MODE_CBC, iv=self.iv)
            ct_bytes = self.cipher.encrypt(p_msg)

            # ECB part
            ecb_obj = AES.new(self.ecb_key, AES.MODE_ECB)
            tag_bytes = ecb_obj.encrypt(ct_bytes)
            tag = b64encode(tag_bytes).decode('utf-8')
            if tag == received_tag:
                return [True, received_tag, tag]
            else:
                return [False, received_tag, tag]

        else:
            print("Please give a non-empty cipher... ")
            return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--operation", "-o", type=str, help="the task: generate or verify")
    parser.add_argument("--input", "-i", help="the message for which tag must be generated or verified.")
    parser.add_argument("--decrypt_key", "-d", type=str, help="the key using which we generate or re-generate tag.")
    args = parser.parse_args()
    if len(sys.argv) > 1:
        if args.operation == "generate":
            msg = args.input
            key_len = 16
            k = key_gen.KeyGen(length=key_len)
            k.get_keys()
            print("key generated is: ", k.key)
            key = k.key
            aes_obj = CBC_MAC(kkey=key)
            encrypted_msg = aes_obj.generate_tag(message=msg)
            print("Encrypted msg in bytes: ", encrypted_msg)
        elif args.operation == "verify" and args.decrypt_key is not None:
            cipher = args.input
            key = args.decrypt_key
            print("Now decrypting: ")
            aes_obj = CBC_MAC(kkey=key)
            decrypted_msg = aes_obj.verify_tag(ccipher=cipher)
            verification = aes_obj.verify_tag(ccipher=cipher)
            if verification[0]:
                print("verified successfully.")
                print("Received Tag: ", verification[1])
                print("Generated Tag: ", verification[2])
            else:
                print("tag verification failed.")
                print("Received Tag: ", verification[1])
                print("Generated Tag: ", verification[2])
            print()
        else:
            print("Invalid operation")
    else:
        print()
        print("You have not passed any commandline parameters. Therefore running default encryption and decryption "
              "processes on default message.")
        default_msg = "Let there be peace!"
        print("Default message to be encrypted: ", default_msg, sep='\n')
        print()

        # CBC MAC encryption part.

        print("First: Demonstration of CBC encryption and decryption.")
        k = key_gen.KeyGen(length=16)
        k.get_keys()
        print("key generated is: ", k.key)
        key = k.key
        aes_obj = CBC_MAC(kkey=key)
        encrypted_msg = aes_obj.generate_tag(default_msg)
        print("Encrypted msg in bytes: ", encrypted_msg)

        # CBC MAC decryption part.

        cipher = encrypted_msg
        print("Now decrypting: ")
        aes_obj = CBC_MAC(kkey=key)
        verification = aes_obj.verify_tag(ccipher=cipher)
        if verification[0]:
            print("tag verified successfully.")
            print("Received Tag: ", verification[1])
            print("Generated Tag: ", verification[2])
        else:
            print("tag verification failed.")
            print("Received Tag: ", verification[1])
            print("Generated Tag: ", verification[2])
        print()
