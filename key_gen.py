import requests
from time import sleep


class KeyGen:
    def __init__(self, length=15, file_store=True):
        self.base_url = "https://www.random.org/strings/"
        self.extension = "?num=1&len="+str(length)+"&digits=on&upperalpha=on&loweralpha=on&unique=on&format=plain&rnd=new"
        self.file_store = file_store
        self.full_url = self.base_url+self.extension
        self.sleep_seconds = 5
        self.key = None

    def get_keys(self):
        response = requests.get(self.full_url)
        if response.status_code == 200:
            self.key = response.text.strip()
        else:
            print("API call to random.org failed; retrying in 5 seconds...")
            sleep(self.sleep_seconds)
            self.get_keys()


if __name__ == "__main__":
    k = KeyGen(length=20)
    k.get_keys()
    print(k.key)