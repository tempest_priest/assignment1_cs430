import key_gen
import argparse
import ast

from hashlib import md5
from base64 import b64decode
from base64 import b64encode

import json
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad


class AESCipherEAX:
    def __init__(self, kkey=None):
        self.key = md5(kkey.encode('utf8')).digest()
        self.cipher = None

    def encrypt(self, message=None):
        if message is not None:
            header_obj = key_gen.KeyGen(length=len(message))
            header_obj.get_keys()
            header = header_obj.key
            header = bytes(header, encoding='utf-8')
            message = bytes(message, encoding='utf-8')
            self.cipher = AES.new(self.key, AES.MODE_EAX)
            self.cipher.update(header)
            ciphertext, tag = self.cipher.encrypt_and_digest(message)
            json_k = ['nonce', 'header', 'ciphertext', 'tag']
            json_v = [b64encode(x).decode('utf-8') for x in (self.cipher.nonce, header, ciphertext, tag)]
            result = json.dumps(dict(zip(json_k, json_v)))
            return result
        else:
            print("Please give a non-empty message... ")
            return None

    def decrypt(self, cipher_obj=None):
        if cipher_obj is not None:
            b64 = json.loads(cipher_obj)
            json_k = ['nonce', 'header', 'ciphertext', 'tag']
            jv = {kk: b64decode(b64[kk]) for kk in json_k}
            self.cipher = AES.new(self.key, AES.MODE_EAX, nonce=jv['nonce'])
            self.cipher.update(jv['header'])
            plaintext = self.cipher.decrypt_and_verify(jv['ciphertext'], jv['tag'])
            return plaintext
        else:
            print("Please give a non-empty cipher... ")
            return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--operation", "-o", type=str, help="the task: encrypt or decrypt")
    parser.add_argument("--input", "-i", help="the message to be encrypted or the cipher to be decrypted")
    parser.add_argument("--decrypt_key", "-d", type=str, help="the key using which we decrypt the cipher")
    args = parser.parse_args()
    if args.operation == "encrypt":
        msg = args.input
        key_len = 16
        k = key_gen.KeyGen(length=key_len)
        k.get_keys()
        print("key generated is: ", k.key)
        key = k.key
        aes_obj = AESCipherEAX(kkey=key)
        encrypted_msg = aes_obj.encrypt(message=msg)
        print("Encrypted msg in bytes: ", encrypted_msg)
    elif args.operation == "decrypt" and args.decrypt_key is not None:
        cipher_object = args.input
        key = args.decrypt_key
        print("Now decrypting: ")
        aes_obj = AESCipherEAX(kkey=key)
        decrypted_msg = aes_obj.decrypt(cipher_obj=cipher_object)
        print("Decrypted msg: ", bytes(decrypted_msg).decode('utf-8'))
    else:
        print("Invalid operation")
